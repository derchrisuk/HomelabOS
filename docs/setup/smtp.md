# SMTP

In order for your applications to be able to send out emails, you
need an SMTP server. A nice free one is [Mailgun](http://mailgun.com/).

Create an account, and follow their steps to register your domain.

Once that is completed, you can plug the settings they provide into
Nextcloud and other apps that send emails.

